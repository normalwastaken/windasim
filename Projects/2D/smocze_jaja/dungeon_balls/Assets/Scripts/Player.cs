﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    // Global Variables

    public float speed;
    Animator anim;
    AudioSource steps;

    // Use this for initialization
    void Start () {

        // Set Variables

        speed = 5;

        // Animator
        anim = GetComponent<Animator>();
        anim.speed = 1;

        // Sounds
        steps = GetComponent<AudioSource>();
        steps.Stop();


    }
	
	// Update is called once per frame
	void Update () {

        Movement();

	}

    void Movement () {

        // Variables

        KeyCode kcode;

        // Movement key mapping

        if (Input.GetKey(KeyCode.W))
            kcode = KeyCode.W;
        else if (Input.GetKey(KeyCode.A))
            kcode = KeyCode.A;
        else if (Input.GetKey(KeyCode.S))
            kcode = KeyCode.S;
        else if (Input.GetKey(KeyCode.D))
            kcode = KeyCode.D;
        else if (Input.GetKey(KeyCode.Space))
            kcode = KeyCode.Space;
        else
            kcode = KeyCode.Keypad0;

        // Movement action

        switch (kcode) {

            case KeyCode.W:
                anim.SetInteger("direction", 1);
                transform.Translate(0, speed * Time.deltaTime, 0);
                if (steps.isPlaying == false)
                    steps.Play();
                break;

            case KeyCode.A:
                anim.SetInteger("direction", 2);
                transform.Translate(-speed * Time.deltaTime, 0, 0);
                if (steps.isPlaying == false)
                    steps.Play();
                break;

            case KeyCode.S:
                anim.SetInteger("direction", 3);
                transform.Translate(0, -speed * Time.deltaTime, 0);
                if (steps.isPlaying == false)
                    steps.Play();
                break;

            case KeyCode.D:
                anim.SetInteger("direction", 4);
                transform.Translate(speed * Time.deltaTime, 0, 0);
                if (steps.isPlaying == false)
                    steps.Play();
                break;

            case KeyCode.Space:
                anim.SetInteger("direction", 0);
                steps.Pause();
                break;

            default:
                anim.SetInteger("direction", 0);
                steps.Pause();
                break;
                
        }

    }

}
