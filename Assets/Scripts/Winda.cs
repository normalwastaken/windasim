﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Winda : MonoBehaviour {

	[SerializeField] float speed = 100f;

	Rigidbody rigidBody;

	// Use this for initialization
	void Start () {
		rigidBody = GetComponent<Rigidbody> ();		
	}
	
	// Update is called once per frame
	void Update () {
		RespondToInput ();		
	}

	void RespondToInput() {
		if (Input.GetKey(KeyCode.W)) {
			rigidBody.AddRelativeForce(Vector3.up * speed);
		}
	}
}
