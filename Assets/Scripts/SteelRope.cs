﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteelRope : MonoBehaviour {

	// interactions
	public Transform whatTheRopeIsConnectedTo;
	public Transform whatIsHangingFromTheRope;

	//line renderer
	private LineRenderer lineRenderer;

	//list with sections
	public List<Vector3> allRopeSections = new List<Vector3>();

	//data
	private float ropeLength = 1f;
	private float minRopeLength = 1f;
	private float maxRopeLength = 20f;
	//mass carried by roep
	private float loadMass = 100f;
	//adding rope speed
	float winchSpeed = 2f;

	SpringJoint springJoint;


	// Use this for initialization
	void Start () {
		springJoint = whatTheRopeIsConnectedTo.GetComponent<SpringJoint> ();

		//init line renderer
		lineRenderer = GetComponent<LineRenderer>();

		//INIT Spring to approximate rope from a to b
		UpdateSpring();

		//add weight to winda or else that is carried
		whatIsHangingFromTheRope.GetComponent<Rigidbody>().mass = loadMass;
	}
	
	// Update is called once per frame
	void Update () {
		//add more/less rope
		UpdateWinch();

		//display rope
		DisplayRope();
	}

	//update spring const and the length of spring
	private void UpdateSpring() {
		//density of stainless steel rope
		float density = 7750f;
		//radius
		float radius = 0.05f;

		float volume = Mathf.PI * radius * ropeLength;

		// mass + mass with cargo
		float ropeMass = volume * density;

		ropeMass += loadMass;

		float ropeForce = ropeMass * 9.81f;

		//Use the spring equation to calculate F = k * x should balance this force, 
		//where x is how much the top rope segment should stretch, such as 0.01m

		//Is about 146000
		float kRope = ropeForce / 0.01f;

		//print(ropeMass);

		//Add the value to the spring
		springJoint.spring = kRope * 1.0f;
		springJoint.damper = kRope * 0.8f;

		//Update length of the rope
		springJoint.maxDistance = ropeLength;
	}

	//Display the rope with a line renderer
	private void DisplayRope()
	{
		//This is not the actual width, but the width use so we can see the rope
		float ropeWidth = 0.2f;

		lineRenderer.startWidth = ropeWidth;
		lineRenderer.endWidth = ropeWidth;


		//Update the list with rope sections by approximating the rope with a bezier curve
		//A Bezier curve needs 4 control points
		Vector3 A = whatTheRopeIsConnectedTo.position;
		Vector3 D = whatIsHangingFromTheRope.position;

		//Upper control point
		//To get a little curve at the top than at the bottom
		Vector3 B = A + whatTheRopeIsConnectedTo.up * (-(A - D).magnitude * 0.1f);
		//B = A;

		//Lower control point
		Vector3 C = D + whatIsHangingFromTheRope.up * ((A - D).magnitude * 0.5f);

		//Get the positions
		BezierCurve.GetBezierCurve(A, B, C, D, allRopeSections);


		//An array with all rope section positions
		Vector3[] positions = new Vector3[allRopeSections.Count];

		for (int i = 0; i < allRopeSections.Count; i++)
		{
			positions[i] = allRopeSections[i];
		}

		//Just add a line between the start and end position for testing purposes
		//Vector3[] positions = new Vector3[2];

		//positions[0] = whatTheRopeIsConnectedTo.position;
		//positions[1] = whatIsHangingFromTheRope.position;


		//Add the positions to the line renderer
		lineRenderer.numPositions = positions.Length;

		lineRenderer.SetPositions(positions);
	}

	//Add more/less rope
	private void UpdateWinch()
	{
		bool hasChangedRope = false;

		//More rope
		if (Input.GetKey(KeyCode.O) && ropeLength < maxRopeLength)
		{
			ropeLength += winchSpeed * Time.deltaTime;

			hasChangedRope = true;
		}
		else if (Input.GetKey(KeyCode.I) && ropeLength > minRopeLength)
		{
			ropeLength -= winchSpeed * Time.deltaTime;

			hasChangedRope = true;
		}


		if (hasChangedRope)
		{
			ropeLength = Mathf.Clamp(ropeLength, minRopeLength, maxRopeLength);

			//Need to recalculate the k-value because it depends on the length of the rope
			UpdateSpring();
		}
	}
}
